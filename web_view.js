import React, {Component} from 'react';
import {View, StyleSheet} from 'react-native';
import {WebView} from 'react-native-webview';

const WebViewApp = () => {
    return (
        <View style={styles.container}><WebView
            source={{uri: 'https://dahch.gitlab.io/prueba'}}/></View>
    );
};
export default WebViewApp;

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});
