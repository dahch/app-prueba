import React from 'react';
import WebViewApp from './web_view';

const App = () => {
    return (
        <WebViewApp/>
    );
};

export default App;
